\documentclass[a4paper]{article}
%\documentclass[a4paper,10pt]{article}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
% \usepackage{xfrac}
% \usepackage[all]{xy}
\usepackage{mathtools}
\usepackage{graphicx}
% \usepackage{fullpage}
\usepackage{hyperref}
\usepackage[T1]{fontenc}
\usepackage[utf8x]{inputenc}
\usepackage[italian]{babel}
%\usepackage{lmodern}

% \usepackage{pdftricks}
% \begin{psinputs}
%    \usepackage{pstricks}
%    \usepackage{multido}
% \end{psinputs}

\usepackage{ulem}

\usepackage{tikz}
\usetikzlibrary{arrows}

% \setlength{\parindent}{0in}

% \newcounter{counter1}

% \theoremstyle{plain}
% \newtheorem{myteo}[counter1]{Teorema}
% \newtheorem{mylem}[counter1]{Lemma}
% \newtheorem{mypro}[counter1]{Proposizione}
% \newtheorem{mycor}[counter1]{Corollario}
% \newtheorem*{myteo*}{Teorema}
% \newtheorem*{mylem*}{Lemma}
% \newtheorem*{mypro*}{Proposizione}
% \newtheorem*{mycor*}{Corollario}

% \theoremstyle{definition}
% \newtheorem{mydef}[counter1]{Definizione}
% \newtheorem{myes}[counter1]{Esempio}
% \newtheorem{myex}[counter1]{Esercizio}
% \newtheorem*{mydef*}{Definizione}
% \newtheorem*{myes*}{Esempio}
% \newtheorem*{myex*}{Esercizio}

% \theoremstyle{remark}
% \newtheorem{mynot}[counter1]{Nota}
% \newtheorem{myoss}[counter1]{Osservazione}
% \newtheorem*{mynot*}{Nota}
% \newtheorem*{myoss*}{Osservazione}

\theoremstyle{plain}
\newtheorem{myteo}{Teorema}[section]
\newtheorem{mylem}[myteo]{Lemma}
\newtheorem{mypro}[myteo]{Proposizione}
\newtheorem{mycor}[myteo]{Corollario}
\newtheorem*{myteo*}{Teorema}
\newtheorem*{mylem*}{Lemma}
\newtheorem*{mypro*}{Proposizione}
\newtheorem*{mycor*}{Corollario}

\theoremstyle{definition}
\newtheorem{mydef}[myteo]{Definizione}
\newtheorem{myes}[myteo]{Esempio}
\newtheorem{myex}[myteo]{Esercizio}
\newtheorem*{mydef*}{Definizione}
\newtheorem*{myes*}{Esempio}
\newtheorem*{myex*}{Esercizio}

\theoremstyle{remark}
\newtheorem{mynot}[myteo]{Nota}
\newtheorem{myoss}[myteo]{Osservazione}
\newtheorem*{mynot*}{Nota}
\newtheorem*{myoss*}{Osservazione}


\newcommand{\obar}[1]{\overline{#1}}
\newcommand{\ubar}[1]{\underline{#1}}

\newcommand{\set}[1]{\left\{#1\right\}}
\newcommand{\pa}[1]{\left(#1\right)}
\newcommand{\ang}[1]{\left<#1\right>}
\newcommand{\bra}[1]{\left[#1\right]}
\newcommand{\abs}[1]{\left|#1\right|}
\newcommand{\norm}[1]{\left\|#1\right\|}

\newcommand{\pfrac}[2]{\pa{\frac{#1}{#2}}}
\newcommand{\bfrac}[2]{\bra{\frac{#1}{#2}}}
\newcommand{\psfrac}[2]{\pa{\sfrac{#1}{#2}}}
\newcommand{\bsfrac}[2]{\bra{\sfrac{#1}{#2}}}

\newcommand{\der}[2]{\frac{\partial #1}{\partial #2}}
\newcommand{\pder}[2]{\pfrac{\partial #1}{\partial #2}}
\newcommand{\sder}[2]{\sfrac{\partial #1}{\partial #2}}
\newcommand{\psder}[2]{\psfrac{\partial #1}{\partial #2}}

\newcommand{\intl}{\int \limits}

\DeclareMathOperator{\de}{d}
\DeclareMathOperator{\id}{Id}
\DeclareMathOperator{\len}{len}

\DeclareMathOperator{\gl}{GL}
\DeclareMathOperator{\aff}{Aff}
\DeclareMathOperator{\isom}{Isom}

\title{Giochi su grafi}
\date{\today}
\author{Enrico Polesel}

\begin{document}
\maketitle

\section{Introduzione}
\label{sec:introduzione}

La teoria dei giochi non cooperativi ad informazione completa ci
permette di analizzare situazioni in cui i partecipanti prendono delle
decisioni ed il loro guadagno dipende in modo arbitrario dalla loro
scelta e dalle scelte degli altri partecipanti. In modo più formale
possiamo scrivere:
\begin{itemize}
\item $N = \set{1,...,n}$ è l'insieme dei giocatori
\item $X_i$ è l'insieme delle scelte disponibili per il giocatore $i$
\item $\pa{u_i}_{i\in N}$ con $u_i: \prod _i X_i \to \mathbb{R}$ è
  l'insieme delle funzioni guadagno dei giocatori.
\end{itemize}

\begin{myes}
  \label{es:best-shot}
  Consideriamo un grafo $\pa{N,E}$ (ad esempio quello in figura
  \ref{fig:esempio-best-shot}), chiamiamo $N_i$ l'insieme dei vicini
  del nodo $i$. Dato $0<c<1$ costruiamo un gioco con:
  \begin{itemize}
  \item i nodi $N$ come insieme dei giocatori;
  \item per ogni giocatore le sue strategie sono $X = \set{0,1}$;
  \item per ogni giocatore $i\in N$ la sua funzione guadagno è
    \[ u_i\pa{x_1,...,x_n} = \min\pa{1,x_i + \sum_{j\in N_i} x_j} - c x_i \]
  \end{itemize}
  La funzione guadagno può essere letta come una prima parte di
  ``ricavo'' (che vale $1$ se e solo se almeno uno tra il giocatore $i$
  e i suoi vicini ha scelto l'azione $1$, altrimenti vale $0$) e una
  seconda parte di ``costo'' (pagato quando il giocatore $i$ sceglie
  l'azione $1$.

  Possiamo descrivere la miglior risposta di ogni giocatore come:
  \begin{table}[!ht]
    \centering
    \begin{tabular}{c|c|c}
      comportamento dei vicini & miglior risposta & guadagno \\
      \hline
      almeno uno gioca $1$ & $0$ & $1$ \\
      tutti giocano $0$ & $1$ & $1-c > 0$ 
    \end{tabular}
  \end{table}

  \begin{figure}[ht]
    \centering
    \begin{tikzpicture}[mnode/.style={circle,fill=blue!20},>=latex]
      \node[mnode] (1) at (-4,0) {1};
      \node[mnode] (2) at (-5,2) {2};
      \node[mnode] (3) at (-6,0) {3};
      \node[mnode] (4) at (-4.5,-2) {4};
      \node[mnode] (5) at (-2,-1) {5};
      \node[mnode] (6) at (-2,1) {6};
      \draw [-] (1) to (2);
      \draw [-] (1) to (3);
      \draw [-] (1) to (4);
      \draw [-] (1) to (5);
      \draw [-] (1) to (6);

      \node[mnode] (7) at (0,0) {7};
      \node[mnode] (8) at (2,0) {8};
      \node[mnode] (9) at (1,2) {9};
      \node[mnode] (10) at (1,-2) {10};
      \draw [-] (7) to (8);
      \draw [-] (7) to (9);
      \draw [-] (7) to (10);
      \draw [-] (9) to (8);
      \draw [-] (10) to (8);
    \end{tikzpicture}
    \caption{Un possibile grafo}
    \label{fig:esempio-best-shot}
  \end{figure}

  In particolare possiamo caratterizzare gli equilibri di Nash come i
  ricoprimenti $N' \subseteq N$ massimali del grafo $\pa{N,E}$ tali
  che il sottografo $N', E\cap \pa{N'\times N'}$ sia composto solo da
  nodi isolati.
\end{myes}

L'esempio precedente ci mostra chiaramente alcune caratteristiche dei
giochi che studieremo:
\begin{itemize}
\item il guadagno di ogni giocatore dipende dalla sua azione e dalle
  azioni dei vicini, ma non dalle azioni degli altri giocatori;
\item i vicini sono \textbf{anonimi}: la funzione guadagno dipende solo
  dall'azione compiuta da ogni vicino e non dalla sua identità;
\item il gioco è \textbf{simmetrico}: i guadagni dipendono solo
  dalla struttura di grafo e dalle strategie giocate.
\end{itemize}

I giochi di questo tipo possono avere molti equilibri di Nash (anche
molto diversi tra loro). L'ipotesi di informazione completa implica
che ogni giocatore deve conoscere tutte le funzioni guadagno, quindi
\textbf{tutto lo stato della rete} (cioè tutto il grafo), ma questo
potrebbe essere difficile da sostenere nel caso di reti molto grandi
come le reti sociali.

Possiamo aspettarci che ogni giocatore possa accedere a delle
informazioni ``locali'' sulla rete, supporremo quindi che la strategia
del giocatore si basi sulla conoscenza del \textbf{proprio grado}
(cioè la cardinalità del suo insieme dei vicini) e su alcune
informazioni (in forma probabilistica) riguardo la \textbf{struttura
  del grafo nelle sue vicinanze}.

Questo tipo di approccio (non necessariamente nella stessa forma che
useremo noi) \`e stato usato per descrivere molti fenomeni sociali, ad
esempio nello studio dei fenomeni criminali (in \cite{glaeser1996}
viene mostrato che la decisione di compiere attività criminali,
soprattutto se non gravi, dipende anche dalle scelte dei vicini), del
mercato del lavoro\cite{topa2001} e dell'innovazione tecnologica (in
\cite{conley2010} viene studiata la diffusione di tecniche agricole in
Ghana).

Nella prossima sezione introdurremmo il nostro modello di rete sociale
e definiremo i suoi equilibri di Nash concentrandoci sugli equilibri
\textbf{simmetrici} di strategie miste, successivamente nella sezione
\ref{sec:risultati} mostreremo l'esistenza di tali equilibri alcune
caratteristiche aggiuntive in presenza di ulteriori ipotesi sulla
struttura del grafo, daremo anche dei brevi cenni sull'evoluzione
degl'equilibri in seguito alla modifica della struttura del gioco.

\section{Modello e definizioni}
\label{sec:modello}

Descriveremo i nostri giochi come una quadrupla
\[ \pa{N,X,\set{v_k}_{k\in \set{0,...,n-1}}, \mathbf{P}} \]
dove $N$ è l'insieme dei giocatori, $X$ le loro strategie disponibili,
$v_k$ generano le funzioni guadagno dei singoli giocatori e
$\mathbf{P}$ racchiude l'informazione stocastica sulla struttura del
grafo disponibile ai giocatori.

Tutti gli oggetti che definiremo sono simmetrici e anonimi nei
giocatori (possono dipendere dalle loro scelte o dai loro gradi, ma
non dalle loro identità), per descriveremo le strategie solo in
funzione del grado dei giocatori. Siamo interessati a
\textbf{strategie miste}.

\begin{mynot}
  Nel modello non abbiamo inserito gli archi del grafo, possiamo
  immaginare che ogni giocatore scelga una strategia mista per ogni
  possible grado \textbf{prima che si formi il grafo}, dopo la
  costruzione del grafo il giocatore verrà informato del suo grado e
  in base a questo giocherà una strategia.
\end{mynot}

\subsection{Insieme dei giocatori e struttura di grafo}

In modo analogo all'esempio \ref{es:best-shot} consideriamo un grafo
$G = \pa{N,E}$ con insieme dei nodi $N = \set{1,..,n}$ e matrice di
adiacenza $g\in \set{0,1}^{n\times n}$, chiamiamo
$N_i = N_i\pa{g} = \set{j\mid g_{ij}=1}$ l'insieme dei vicini del nodo
$i$, denotiamo con $k_i = k_i\pa{g} = \abs{N_i\pa{g}}$ il grado del
nodo $i$.

Prendiamo l'insieme dei nodi $N$ come insieme dei giocatori, assegniamo
ad ognuno lo stesso insieme di strategie giocabili
$X \subseteq \bra{0,1}$, supponiamo che $X$ sia compatto, che
$0,1\in X$ e che sia o discreto o connesso.

\subsection{Funzioni guadagno}

Dato un profilo di strategie $\mathbf{x}\in X^n$, per ogni giocatore
la sua funzione guadagno sarà data da
\[ u_i(\mathbf{x}) = v_{k_i\pa{g}}\pa{x_i,\mathbf{x}_{N_i\pa{g}}} \]
dove:
\begin{itemize}
\item $v_k : X \times X^k \to \mathbb{R}$;
\item $\mathbf{x}_{N_i\pa{g}}$ sono le componenti di $\mathbf{x}$
  relative ai vicini di $i$;
\item se $\mathbf{x'}\in X^k$ è una permutazione di
  $\mathbf{x}\in X^k$ allora
  $v_k(x_i,\mathbf{x}) = v_k(x_i,\mathbf{x'})$;
\item se $X$ è connesso allora $v$ è continua in tutte le componenti e
  concava nella prima ($x_i$).
\end{itemize}

In questo modo il guadagno di un giocatore dipende solo da:
\begin{itemize}
\item il grado $k_i$ del giocatore,
\item l'azione del giocatore,
\item le azioni dei vicini;
\end{itemize}
in particolare non dipende da:
\begin{itemize}
\item l'identità del giocatore (due giocatori con lo stesso grado
  hanno la stessa $v_k$),
\item l'identità dei vicini o il loro ordinamento.
\end{itemize}

Quando $\mathbf{x},\mathbf{x'}\in X^k$ sono vettori scriviamo
$\mathbf{x}\ge \mathbf{x'}$ quando la relazione vale componente per
componente. Alcune classi di funzioni guadagno possono soddisfare
anche le seguenti definizioni.

\begin{mydef}[Strategic complements]
  \label{def:strategic-complements}
  Le funzioni $\pa{v_k}_{k}$ esibiscono \textit{strategic complements}
  se $\forall k$, $\forall x_i > x'_i$ e
  $\forall \mathbf{x},\mathbf{x}'\in X^k, \mathbf{x}\ge \mathbf{x'}$
  si ha:
  \[ v_k\pa{x_i,\mathbf{x}} - v_k\pa{x'_i,\mathbf{x}} \ge
    v_k\pa{x_i,\mathbf{x'}} - v_k\pa{x'_i,\mathbf{x'}} \]
  diciamo che la relazione è stretta se vale la disuguaglianza stretta
  ogni volta che $\mathbf{x}\neq \mathbf{x'}$.
\end{mydef}
\begin{mydef}[Strategic substitutes]
  \label{def:strategic-substitutes}
  Le funzioni $\pa{v_k}_{k}$ esibiscono \textit{strategic substitutes}
  se $\forall k$, $\forall x_i > x'_i$ e
  $\forall \mathbf{x},\mathbf{x'}\in X^k, \mathbf{x}\ge \mathbf{x'}$
  si ha:
  \[ v_k\pa{x_i,\mathbf{x}} - v_k\pa{x'_i,\mathbf{x}} \le
    v_k\pa{x_i,\mathbf{x'}} - v_k\pa{x'_i,\mathbf{x'}} \]
  diciamo che la relazione è stretta se vale la disuguaglianza stretta
  ogni volta che $\mathbf{x}\neq \mathbf{x'}$.
\end{mydef}

Per capire meglio queste definizioni possiamo pensare a giochi con
$X=\set{0,1}$ (ma le stesse idee si applicano ad istanze
generiche). Il concetto di \textit{strategic complements} ricorda i
giochi di coordinamento: è più vantaggioso fare una certa azione se i
vicini giocano in modo simile. Al contrario quando si hanno
\textit{strategic substitutes} quando un giocatore ottiene un
vantaggio ``gratuitamente'' se i suoi vicini compiono una certa azione
(senza che la debba compiere anche lui), questo capitava nell'esempio
\ref{es:best-shot}.

\begin{mydef}[Positive and negative externalities]
  \label{def:externalities}
  Diciamo che i guadagni hanno \textit{positive externalities} se
  $\forall k$, $\forall x_i \in X$ e
  $\forall \mathbf{x},\mathbf{x'}\in X^k, \mathbf{x}\ge \mathbf{x'}$
  si ha
  \[ v_k\pa{x_i,\mathbf{x}} \ge v_k\pa{x_i,\mathbf{x'}} \]
  
  Analogamente i guadagni hanno \textit{negative externalities} se
  $\forall k$, $\forall x_i \in X$ e
  $\forall \mathbf{x},\mathbf{x'}\in X^k, \mathbf{x}\ge \mathbf{x'}$
  si ha
  \[ v_k\pa{x_i,\mathbf{x}} \le v_k\pa{x_i,\mathbf{x'}} \]

  I comportamenti si dicono \textit{stretti} se vale la disuguaglianza
  ogni volta che $\mathbf{x}\neq \mathbf{x'}$.
\end{mydef}
Le \textit{externalities} (quando presenti) ci indicano come il
guadagno di un giocatore viene influenzato dalle strategie dei suoi
vicini.

\begin{mydef}[Proprietà A]
  \label{def:prop-A}
  Diciamo che i guadagni soddisfano la \textit{proprietà A} se
  $\forall k$, $\forall x_i\in X$, $\forall \mathbf{x}\in X^k$ si ha:
  \[ v_{k+1}\pa{x_i,\pa{\mathbf{x},0}} = v_k\pa{x_i,\mathbf{x}} \]
\end{mydef}
Quando vale questa proprietà si ha che avere un vicino che gioca $0$ è
come non avere tale vicino.

Vediamo alcuni esempi di funzioni guadagno.

\begin{myes}[Guadagni dipendenti dalla somma]
  Data $f$ non decrescente consideriamo le funzioni
  \[ v_k(x_i,\mathbf{x}) = f\pa{ x_i + \lambda \sum _{j=1}^k x_j } -
    c\pa{x_i} \]

  Si vede facilmente che si hanno \textit{positive externalities} se
  $\lambda > 0$ e \textit{negative externalities} se $\lambda <0$.

  Si può vedere che se $f$ è derivabile due volte allora queste
  funzioni esibiscono \textit{strategic complements} se $\lambda f''
  \ge 0$ e \textit{strategic substitutes} se $\lambda f'' \le 0$
  (applicando il teorema fondamentale del calcolo integrale alla
  relazione da dimostrare).

  \`E facile vedere che questa classe di funzioni soddisfa anche la
  \textit{Proprietà A}.

  L'esempio \ref{es:best-shot} è in questa forma, esso esibiva
  \textit{strategic substitutes} e \textit{positive externalities}.
\end{myes}

\begin{myes}[Guadagni dipendenti dalla media]
  Sia $X = \set{0,1}$ e $f$ una funzione crescente, consideriamo le
  funzioni
  \[ v_k(x_i,\mathbf{x}) = x_i f\pa{ \frac{\sum _{j=1}^k x_j}{k} } -
    c\pa{x_i} \]

  Queste funzioni hanno \textit{positive externalities} e
  \textit{strategic complements}, ma non soddisfano la
  \textit{Proprietà A}.
\end{myes}


\subsection{Informazioni (incomplete) sul grafo}

Come abbiamo già anticipato, i giocatori non conoscono tutto il grafo
(in particolare non ne conoscono l'insieme degli archi), ma conoscono
solo il loro grado e alcune informazioni sulla struttura del grafo.

Supponiamo che i giocatori conoscano per ogni possibile grado $k$ la
distribuzione di probabilità su $\mathbb{N}^k$ dei possibili gradi dei
vicini di un nodo di grado $k$. Cioè supponiamo che sia noto l'insieme

\[ \mathbf{P} \equiv \set{ \bra{ P\pa{\mathbf{k} \mid k}}_{\mathbf{k}\in
      \mathbb{N}^k}}_{k\in \mathbb{N}} \]

\begin{mynot}
  In questa definizione e per il resto della sezione ci riferiamo a
  distribuzioni (e funzioni) su $\mathbb{N}^k$, in realtà potremmo
  considerare solo $\set{0,...,n-1}^k$ visto che i gradi dei nodi sono
  limitati.
\end{mynot}

Per semplicità scriviamo anche:
\[ P_k\pa{ \mathbf{k}} = P\pa{\mathbf{k} \mid k} \] 

Trattandosi di distribuzioni discrete possiamo scrivere il valore
atteso di una funzione $f: \mathbb{N}^k \to \mathbb{R}$ come:
\[ \mathbb{E}_{P\pa{ \cdot \mid k}} \bra{f} = \sum _{\mathbf{k}\in
    \mathbb{N}} P\pa{\mathbf{k}\mid k}f\pa{\mathbf{k}} \]

Generalizziamo al caso di funzioni $f: \mathbb{N}^m \to \mathbb{R}$
con $m\le k$ troncando i vettori $\mathbf{k} = \pa{
  k_1,k_2,...,k_k}$ alle prime $m$ componenti:
\[ E_{P\pa{ \cdot \mid k}} \bra{f} = \sum _{\mathbf{k}\in
    \mathbb{N}} P\pa{\mathbf{k}\mid k}f\pa{k_1,...,k_m} \]

La dipendenza della distribuzione dal grado $k$ del nodo ci permette
di descrivere situazioni di correlazione (sia negativa che positiva)
tra il grado di un nodo e i gradi dei suoi vicini, ad esempio in
alcuni casi reali si osserva che i vicini di un nodo con un grado
alto tendono anch'essi ad avere un grado alto.

\begin{mydef}[Positive neighbour affiliation]
  \label{def:positive-affiliation}
  Diciamo che le distribuzioni $\mathbf{P}$ esibiscono
  \textit{positive neighbour affiliation} se per ogni $k'>k$ e $f:
  \mathbb{N}^k \to \mathbb{R}$ non decrescente si ha
  \[ E_{P\pa{ \cdot \mid k'}} \bra{f} \ge E_{P\pa{ \cdot \mid k}}
    \bra{f} \]
\end{mydef}
\begin{mydef}[Negative neighbour affiliation]
  \label{def:negative-affiliation}
  Diciamo che le distribuzioni $\mathbf{P}$ esibiscono
  \textit{negative neighbour affiliation} se per ogni $k'>k$ e $f:
  \mathbb{N}^k \to \mathbb{R}$ non decrescente si ha
  \[ E_{P\pa{ \cdot \mid k'}} \bra{f} \le E_{P\pa{ \cdot \mid k}}
    \bra{f} \]
\end{mydef}

Ad esempio si può vedere facilmente che se i gradi dei nodi sono
indipendenti allora sono verificate entrambe le definizioni.

Il concetto di \textit{neighbour affiliation} ci dà una sorta di
correlazione tra il grado di un nodo e il grado dei propri vicini, ad
esempio quando si ha \textit{positive neighbour affiliation} un nodo
con un grado alto tende ad avere vicini di grado alto.

\subsection{Evoluzione dei grafi}

Siamo anche interessati ad analizzare come cambiando gli equilibri
aggiungendo o togliendo archi al grafo, per farlo dobbiamo tradurre
questa operazione in termini della famiglia di probabilità
$\mathbf{P}$.

\begin{mydef}
  \label{def:p-dominazione}
  Dati due insiemi $\mathbf{P}$ e $\mathbf{P'}$ come sopra diciamo che
  \textit{$\mathbf{P'}$ domina $\mathbf{P}$} se $\forall k\in
  \mathbb{N}$ e per ogni funzione non decrescente $f: \mathbb{N}^k \to
  \mathbb{R}$ si ha
  \[ \mathbb{E}_{P'\pa{\cdot\mid k}}\bra{f} = E_{P'\pa{\cdot\mid
        k}}\bra{f} \ge E_{P\pa{\cdot\mid k}}\bra{f} =
    \mathbb{E}_{P\pa{\cdot\mid k}}\bra{f} \]
\end{mydef}

Questa è una generalizzazione al nostro caso (insiemi di distribuzioni
a più variabili) del concetto di dominanza stocastica del primo
ordine.

\subsection{Strategie}

Come si comporta il giocatore $i \in N$? Conoscendo il modello
$\pa{N,X,\set{v_k}_k, \mathbf{P}}$ e il suo grado $k_i$ giocherà una
strategia mista $\sigma _i(k_i) \in \Delta\pa{X}$ (dove con
$\Delta\pa{X}$ indichiamo l'insieme delle distribuzioni di probabilità
su $X$).

Un profilo di strategie è dato da $\pa{ \sigma _i } _{i\in N}$ con
$\sigma _i : \mathbb{N} \to \Delta (X)$, prendiamo in considerazione
profili di strategie \textbf{simmetrici}, cioè dove tutti i giocatori
hanno la stessa funzione $\sigma : \mathbb{N} \to \Delta (X)$.

Dato $k \in \mathbb{N}$ e $\mathbf{k}\in \mathbb{N}^k$ chiamiamo (con
un piccolo abuso di notazione) $\sigma\pa{\mathbf{k}}$ la
distribuzione di probabilità su $X^k$ la cui componente $j$-esima è
data da $\sigma \pa{k_j}$.

Il guadagno atteso del giocatore $i\in N$ di grado $k_i$ che gioca la
strategia $x_i$ quando gli altri giocatori giocano il profilo di
strategie simmetrico $\pa{\sigma }_{i\in N\setminus \set{i}}$ \`e dato
da
\begin{align*}
  U\pa{x_i, \sigma, k_i} & = \int _{\mathbf{k}\in \mathbb{N}^{k_i},
                           \mathbf{x}\in X^{k_i}} v_{k_i}\pa{ x_i,
                           \mathbf{x}} \de \sigma \pa{ \mathbf{k}
                           }\pa{ \mathbf{x}} \de P \pa{ \mathbf{k}
                           \mid k_i} \\
                         & = \int _{\mathbf{x}\in X^{k_i}} v_{k_i}\pa{
                           x_i, \mathbf{x}} \de \psi \pa{ \mathbf{x},
                           \sigma, k_i }
\end{align*}
dove $\psi \pa{ \mathbf{x}, \sigma, k_i}$ rappresenta la distribuzione
di probabilità su $\mathbf{x}\in X^{k_i}$ indotta da $P\pa{\cdot
  ,k_i}$ e $\sigma$.

\begin{mydef}[Miglior risposta]
  Diciamo che $x_i\in X$ \`e \textit{miglior risposta} del giocatore
  $i\in N$ (di grado $k_i$) alla strategia simmetrica $\sigma$ se
  massimizza la funzione $U\pa{ \cdot, \sigma, k_i}$.
\end{mydef}

Analogamente al caso generale con informazione completa, il concetto
di miglior risposta ci permette di definire gli equilibri di Nash nel
caso di strategie simmetriche.

\begin{mydef}[Equilibrio di Nash]
  $\sigma : \mathbb{N} \to X$ \`e un \textit{equilibrio di Nash} se
  $\forall k \in \mathbb{N}$ si ha che ogni $x\in X$ nel supporto di
  $\sigma(k)$ \`e miglior risposta, cio\`e:
  \[ \forall x \in \mathrm{supp}\pa{\sigma\pa{k}}, \forall x' \in
    X\;\;\; U\pa{x,\sigma, k} \ge U \pa{ x', \sigma, k} \]
\end{mydef}

Potremmo anche analizzare il caso di equilibri non simmetrici (in cui
due giocatori con lo stesso grado giocano strategie miste diverse), ma
questo complicherebbe la nostra trattazione. Si ha che sotto alcune
condizioni gli equilibri sono tutti simmetrici, l'ipotesi pi\`u
importante della prossima proposizione \`e che la dimensione del gioco
deve essere molto grande.

\begin{mypro}
  \label{pro:solo-simmetrici}
  Se valgono le seguente condizioni:
  \begin{enumerate}
  \item il meccanismo con cui viene formata la rete è anonimo;
  \item la popolazione \`e molto grande;
  \item le funzioni guadagno sono strettamente concave nell'azione del
    giocatore;
  \end{enumerate}
  allora tutti gli equilibri del gioco sono simmetrici, in particolare
  dipendono solo dal grado del giocatore.
\end{mypro}

Non vediamo la dimostrazione di questa proposizione. L'idea che sta
alla base della dimostrazione è utilizzare i primi due punti per
mostrare che tutti i giocatori con lo stesso grado devono risolvere lo
stesso problema decisionale e concludere osservando che la strategia
ottima è unica a causa della stretta concavità.

\begin{mydef}[Strategia non decrescente]
  \label{def:strategia-nondec}
  Una strategia $\sigma$ si dice \textit{non decrescente} se $\forall
  k' > k$ si ha che $\sigma (k')$ domina stocasticamente al primo
  ordine $\sigma (k)$.
\end{mydef}
\begin{mydef}[Strategia non crescente]
  \label{def:strategia-noncre}
  Una strategia $\sigma$ si dice \textit{non crescente} se $\forall
  k' > k$ si ha che $\sigma (k')$ è dominata stocasticamente al primo
  ordine da $\sigma (k)$.
\end{mydef}

\begin{mydef}[Degree complementarity]
  \label{def:degree-comp}
  Il guadagno atteso mostra \textit{degree complementarity} se per
  ogni $x_i > x'_i$, $k_i > k'_i$ e $\sigma$ non decrescente si ha
  \[ U\pa{x_i, \sigma, k_i} - U\pa{x'_i, \sigma, k_i} \ge U\pa{x_i,
      \sigma, k'_i} - U\pa{x'_i, \sigma, k'_i}\]
\end{mydef}
\begin{mydef}[Degree substitution]
  \label{def:degree-sub}
  Il guadagno atteso mostra \textit{degree substitution} se per
  ogni $x_i > x'_i$, $k_i > k'_i$ e $\sigma$ non crescente si ha
  \[ U\pa{x_i, \sigma, k_i} - U\pa{x'_i, \sigma, k_i} \le U\pa{x_i,
      \sigma, k'_i} - U\pa{x'_i, \sigma, k'_i}\]
\end{mydef}

L'idea della \textit{degree complementarity} è che, quando viene
giocata una strategia non decrescente, se una strategia ``alta'' (nel
senso dell'ordinamento di $X$) \`e pi\`u conveniente di una ``bassa''
per un giocatore con un certo grado, allora la stessa cosa resta vera
per un giocatore con un grado pi\`u alto.

\begin{mypro}
  \label{pro:a-strategic-degree}
  Se in un modello vale la \hyperref[def:prop-A]{propriet\`a A}, le
  $v_k$ esibiscono
  \hyperref[def:strategic-complements]{\textit{strategic complements}}
  e $\mathbf{P}$ ha
  \hyperref[def:positive-affiliation]{\textit{positive neighbour
      affiliation}} allora i guadagni attesi mostrano
  \hyperref[def:degree-comp]{\textit{degree complementarity}}.
\end{mypro}

\begin{proof}
  Sia $\sigma$ una strategia non decrescente e $x_i > x'_i$, allora
  \begin{align*}
    & \; U\pa{x_i, \sigma, k} - U\pa{x'_i, \sigma, k} = \\
    = & \int _{\mathbf{x}\in X^k} \bra{ v_k\pa{x_i,\mathbf{x}} -
        v_k\pa{x'_i,\mathbf{x}}} \de \psi \pa{\mathbf{x},\sigma,k} \\
    = & \int _{\mathbf{x}\in X^k} \bra{
        v_{k+1}\pa{x_i,(\mathbf{x},0)} -
        v_{k+1}\pa{x'_i,(\mathbf{x},0)}} \de \psi
        \pa{\mathbf{x},\sigma,k} \\
    \le & \int _{\mathbf{x}\in X^k} \bra{
          v_{k+1}\pa{x_i,(\mathbf{x},0)} -
          v_{k+1}\pa{x'_i,(\mathbf{x},0)}} \de \psi
          \pa{(\mathbf{x},0),\sigma,k+1} \\
    \le & \int _{(\mathbf{x},x_{k+1})\in X^{k+1}} \bra{
          v_{k'}\pa{x_i,(\mathbf{x},x_{k+1})} -
          v_{k+1}\pa{x'_i,(\mathbf{x},x_{k+1})}} \de \psi
          \pa{(\mathbf{x},x_{k+1}),\sigma,k+1} \\
    = & \; U\pa{x_i, \sigma, k+1} - U\pa{x'_i, \sigma, k+1} \\
  \end{align*}

  Applicando questo risultato per induzione si ottiene la tesi per $k'
  > k$.
\end{proof}

\begin{mypro}
  Se in un modello vale la propriet\`a A, le $v_k$ esibiscono
  \textit{strategic substitutes} e $\mathbf{P}$ ha \textit{negative
    neighbour affilation} allora i guadagni attesi mostrano
  \textit{degree substitution}.
\end{mypro}


\section{Risultati}
\label{sec:risultati}

Continueremo anche in questa sezione a considerare solo equilibri
simmetrici, ricordiamo che possono esistere anche equilibri non
simmetrici (vedi proposizione \ref{pro:solo-simmetrici}) ad esempio
nel caso di reti piccole.

\subsection{Esistenza e proprietà dell'equilibrio}

Iniziamo enunciando due semplici teoremi di esistenza.

\begin{myteo}[{\cite[Proposition 1]{galeotti2009}}]
  Esiste sempre un equilibrio simmetrico.
\end{myteo}

\begin{myteo}[{\cite[Proposition 1]{galeotti2009}}]
  Se il gioco mostra \hyperref[def:degree-comp]{\textit{degree
      complementarity}} allora esiste un equilibrio simmetrico
  composto da \textbf{strategie pure}.
\end{myteo}

Entrambi questi risultati si possono dimostrare con tecniche standard
di punto fisso su un insieme compatto sfruttando la concavità delle
funzioni guadagno nell'azione del giocatore.

Vediamo ora dei risultati che prevedono opportune proprietà delle
soluzioni (o di almeno una di esse) in funzione delle proprietà del
gioco da analizzare.

\begin{myteo}[{\cite[Proposition 1]{galeotti2009}}]
  Se un gioco mostra \hyperref[def:degree-comp]{\textit{degree
      complementarity}} (o \hyperref[def:degree-sub]{\textit{degree
      substitution}}) allora esiste un equilibrio simmetrico
  \hyperref[def:strategia-nondec]{non decrescente} (o
  \hyperref[def:strategia-noncre]{non crescente}).
\end{myteo}
\begin{proof}
  Dimostriamo il caso di \textit{degree complementarity}, l'altro caso
  si fa in modo analogo.

  Dato un giocatore $i\in N$ e una strategia non decrescente $\sigma$
  giocata dagl'altri giocatori abbiamo (per ipotesi) che per ogni
  $x_i>x'_i$ e $k_i > k'_i$
  \[ U\pa{x_i, \sigma, k_i} - U\pa{x'_i, \sigma, k_i} \ge U\pa{x_i,
      \sigma, k'_i} - U\pa{x'_i, \sigma, k'_i}\]
  ma allora se $x'_i$ era miglior risposta per $k'_i$ si ha che ogni
  miglior risposta per $k_i$ deve essere maggiore o uguale a
  $x'_i$. Quindi ogni miglior strategia data da miglior risposte per
  $i$ deve essere non decrescente.

  L'insieme delle strategie (miste) non decrescenti è convesso e
  compatto, quindi possiamo sempre trovare un equilibrio di Nash
  simmetrico dato da una strategia non decrescente.
\end{proof}

Potremmo aspettarci di avere equilibri monotoni ogni qual volta si ha
\textit{degree complementarity} o \textit{degree substitution}, invece
ci sono degli esempi in cui esistono anche equlibri di Nash non
monotoni nonostante una di tale ipotesi sia soddisfatta.

È possibile dimostrare la monotonia di tutti gli equilibri sotto altre
ipotesi.

\begin{myteo}[{\cite[Proposition 2]{galeotti2009}}]
  \label{teo:tutti-simmetrici-monotoni}
  Se le funzioni guadagno soddisfano la
  \hyperref[def:prop-A]{\textit{proprietà A}}, esibiscono
  \textbf{strettamente}
  \hyperref[def:strategic-complements]{\textit{strategic complements}}
  (o
  \hyperref[def:strategic-substitutes]{\textit{strategic
      substitutes}}) e i gradi dei nodi sono indipendenti allora
  \textbf{tutti} gli equilibri simmetrici sono non decrescenti (o non
  crescenti).
\end{myteo}

Si può osservare che con queste ipotesi si può ricavare che i guadagni
attesi mostrano \textit{degree complimentarity} (usano la proposizione
\ref{pro:a-strategic-degree})) e quindi è possibile applicare il
teorema precedente (che però non ci porta alla tesi).

\begin{myes}[Giochi binari di \textit{substitutes}]
  \label{es:binary-sub}
  Consideriamo un gioco binario (cioè in cui $X = \set{0,1}$) dove le
  funzioni guadagno soddisfano la proprietà A, mostrano \textit{strict
    strategic substitutes} e le distribuzioni $\mathbf{P}$ mostrano
  \hyperref[def:negative-affiliation]{\textit{negative neighbour
      affiliation}}.

  Se avessimo l'indipendenza dei gradi il teorema
  \ref{teo:tutti-simmetrici-monotoni} ci permetterebbe di dire che
  tutti gli equilibri simmetrici sono non crescenti. In questo caso
  particolare è possibile dimostrare (vedi \cite[Proposition
  4]{galeotti2009}) che esiste un unico equilibrio simmetrico, esso
  sar\`a non crescente e, detto $\sigma$ tale equilibrio, si ha che
  esiste $t\in {0,...,n-1}$ tale che:
  \begin{align*}
    \mathbb{P}\pa{\sigma \pa{k} =1} & = 1 & k < t \\
    \mathbb{P}\pa{\sigma \pa{k} =1} & \in \left( 0,1 \right] & k = t \\
    \mathbb{P}\pa{\sigma \pa{k} =1} & =0 & k> t
  \end{align*}
  chiamiamo $t$ \textbf{soglia} associata al gioco.
\end{myes}

Chiudiamo la sezione con un teorema che ci mostra, in alcuni casi
particolari, come variano i guadagni attesi in funzione del grado dei
nodi.

\begin{myteo}[{\cite[Proposition 3]{galeotti2009}}]
  Supponiamo che i guadagni soddisfino la \textit{proprietà A}, che le
  distribuzioni $\mathbf{P}$ mostrino \textit{positive neighbour
    affiliation} e il gioco abbia \textit{positive externalities}
  allora in ogni equilibrio simmetrico non decrescente i guadagni
  attesi sono non decrescenti nel grado del nodo.

  Mantenendo l'ipotesi di avere la \textit{proprietà A} è possibile
  dimostrare le seguenti varianti del teorema:
  
  \begin{tabular}[ht]{c c c | c}
    Neigh. affiliation & Externalities & Monotonia di $\sigma$ & Guadagno atteso \\
    \hline
    Positive & Positive & non decrescente & non decrescente \\
    Positive & Negative & non decrescente & non crescente \\
    Negative & Positive & non crescente & non decrescente \\
    Negative & Negative & non crescente & non crescente 
  \end{tabular}
\end{myteo}

Questo teorema è interessante perché ci mostra in quali casi è più
conveniente avere molte connessioni sociali (cioè un grado più alto) e
quando, invece, conviene essere più isolati.

Nell'esempio \ref{es:binary-sub} abbiamo per ipotesi la
\textit{proprietà A}, \textit{negative neighbour affiliation} e
abbiamo visto che gli equilibri sono tutti non crescenti, quando
abbiamo anche \textit{positive externalities} (come nell'esempio
\ref{es:best-shot}) possiamo concludere che il guadagno atteso di un
nodo è non decrescente. Quindi in questa situazione conviene avere più
connessioni sociali.

\subsection{Evoluzione delle reti}

Data una rete possiamo definire quali sono i suoi equilibri, ci
chiediamo ora come questi possano cambiare quando modifichiamo gli
archi del grafo, cioè quando modifichiamo le informazioni contenute in
$\mathbf{P}$.

Con la definizione \ref{def:p-dominazione} abbiamo introdotto un modo
per confrontare due insiemi di distribuzioni $\mathbf{P}$ e
$\mathbf{P}'$, questo ci introduce una relazione d'ordine non totale,
infatti esistono insiemi non confrontabili.

Concentriamoci nel caso dell'esempio \ref{es:binary-sub}, cosa succede
se aggiungiamo degl'archi al grafo? A parità di grado $k$ un nodo si
aspetta di veder crescere il grado dei vicini (quindi la nuova
distribuzione $P_k$ dominerà la vecchia distribuzione $P'_k$) e
quindi, per monotonia, si aspetterà un'azione minore da parte loro
(cioè una probabilità minore che almeno uno di loro scelga l'azione
$1$). Il comportamento è dato dal prossimo teorema:

\begin{myteo}[{\cite[Proposition 5]{galeotti2009}}]
  Siano $\pa{N,X, \set{v_k}_k, \mathbf{P}}$ e
  $\pa{N,X, \set{v_k}_k, \mathbf{P}'}$ due giochi binari di
  \textit{substitutes} e siano rispettivamente $t$ e $t'$ le soglie
  dei loro unici equilibri simmetrici. Se $\mathbf{P}$ domina
  $\mathbf{P}'$ allora $t\ge t'$.
\end{myteo}

Questo risultato non può essere applicato a molti casi in cui non c'è
una relazione di dominazione. Nell'ipotesi di indipendenza dei gradi
dei nodi vicini è possibile generalizzarlo analizzando le funzioni di
ripartizione in un intorno della soglia\cite[Proposition
6]{galeotti2009}, ma noi non lo vedremo.

Abbiamo visto il caso di giochi binari di \textit{substitutes}, può
essere anche studiato il caso di giochi (non necessariamente binari)
che mostrano \textit{strategic complements}\cite[Sezione 5.2]{galeotti2009}.


\section{Conclusioni}
\label{sec:conclusioni}

Il modello che abbiamo visto permette di modelizzare molti casi di
giochi reali la cui struttura si basa su un grafo (come le reti
sociali) ad informazione incompleta, a seconda della struttura dei
giochi siamo anche in grado di descrivere alcune caratteristiche
dell'equilibrio raggiunto da tali reti almeno al caso asintotico di
reti molto grandi.

Importante è stata la scelta iniziale su ``quanta'' informazione dare
ai giocatori, noi abbiamo scelto di dare ad ogni giocatore un'idea
stocastica sulla possibile struttura del grafo e fargli conoscere il
proprio grado. Una possibile estensione al nostro modello consiste nel
dire al giocatore anche i gradi dei suoi vicini (oltre al suo)
aumentando così il ``raggio'' dell'informazione\cite[Sezione
6]{galeotti2009}.


\bibliographystyle{ieeetr}

\bibliography{relazione}

\end{document}